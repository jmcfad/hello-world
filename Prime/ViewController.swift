
//
//  ViewController.swift
//  Prime
//
//  Created by Mom on 3/6/16.
//  Copyright © 2016 jmcfad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var isPrimeAnswer: UILabel!
    @IBOutlet weak var primeListAnswer: UILabel!
    @IBOutlet weak var isPrimeUserInput: UITextField!

    
    private func addDoneButtonTo(textField: UITextField) {
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "didTapDone:")
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
    }
    
    func didTapDone(sender: AnyObject?) {
        isPrimeUserInput.endEditing(true)
        print("did tap done")
        calculateIsPrime()
    }
    
    func calculateIsPrime() {
        isPrimeAnswer.text = "Yes"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addDoneButtonTo(isPrimeUserInput)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

